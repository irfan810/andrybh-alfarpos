#!/bin/bash

# variable
DOMAIN=$1
NGINX=$(sudo nginx -v)
CERTBOT=$(sudo certbot --version)
CERTIFICATE=$(sudo certbot certificates | grep $DOMAIN)

# Functions
install_nginx(){
    sudo apt-get update && \
    sudo apt-get install nginx snapd -y
}

install_certbot(){
    sudo apt-get update && \
    sudo snap install core; sudo snap refresh core && \
    sudo snap install --classic certbot && \
    sudo ln -s /snap/bin/certbot /usr/bin/certbot
}

wildcard_ssl(){
    sudo snap set certbot trust-plugin-with-root=ok && \
    sudo snap install certbot-dns-google && \
    sudo certbot certonly --dns-google --dns-google-credentials /home/lutfi.tanjung/secret/certbot.json -d $DOMAIN
}

# run function
if [[ $NGINX == *"nginx version"* ]]; then
    install_nginx
fi

if [[ $CERTBOT == *"certbot "* ]]; then
    install_certbot
fi

if [[ $CERTIFICATE == *"Domains: $DOMAIN"* ]]; then
    wildcard_ssl
fi